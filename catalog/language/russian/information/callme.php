<?php
$_['email_subject']  = 'Обратный звонок';

$_['entry_name']     = 'Имя';
$_['entry_phone']    = 'Телефон';
$_['entry_comment']  = 'Сообщение';
$_['entry_captcha']  = 'Проверочный код';

$_['text_submited']  = 'Ваш запрос отправлен';

$_['entry_send']  = 'Жду звонка';
$_['entry_title_call']  = 'Мы всегда рады вам помочь!';
$_['entry_text_call']  = 'Для заказа звонка укажите Ваши данные';

