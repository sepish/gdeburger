<?php
// Text
$_['text_items']    = '<div class="bag"><span>%s</span></div><div class="text-bag"><span>Корзина</span><span class="sum_total">%s</span></div>';
$_['text_empty']    = 'Ваша корзина пуста!';
$_['text_cart']     = 'Посмотреть корзину';
$_['text_checkout'] = 'Оформить заказ';
$_['text_recurring']  = 'Платежный профиль';
