<?php 
class ControllerCommonCategoryPopup extends Controller {
        public function index() {
            
            $this->load->model('catalog/product');
            $this->load->model('catalog/category');
            $this->load->model('tool/image');
            $this->load->language('product/category');
            if ($this->request->server['REQUEST_METHOD'] == 'POST' || $this->request->server['REQUEST_METHOD'] == 'GET'){
                
                if(isset($this->request->get['id']) && $this->request->get['id']){
                    
                    $data['category_info'] = $this->model_catalog_product->getCategory($this->request->get['id']);
                    
                    if($data['category_info']['image_big']){
                        $data['image_category'] = $this->model_tool_image->cropsize($data['category_info']['image_big'],975,545);
                    }else{
                        $data['image_category'] = $this->model_tool_image->cropsize("placeholder.png",975,545);
                    }
                    
                    $filter = array(
                        'filter_category_id' => $this->request->get['id'],
                        'start'              => 0,
                        'limit'              => $this->config->get('config_product_limit')
                        
                    );
                    $data['text_empty'] = $this->language->get('text_empty');
                    
                    $products = $this->model_catalog_product->getProducts($filter);
                    $data['products'] = array();
                    foreach($products as $result){
                        
                        if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                            } else {
				$price = false;
			}
                        
                        if ($result['image']) {
				$image = $this->model_tool_image->cropsize($result['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
                                $image_small = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                            } else {
				$image = $this->model_tool_image->cropsize('placeholder.png', $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
                                $image_small = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
			}
                        
                        $data['products'][] = array(
                            'product_id' => $result['product_id'],
                            'name'       => $result['name'],
							'description'=> strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')),
                            'price'      => $price,
                            'thumb'      => $image,
                            'shipping'   => $result['shipping'],
                            'thumb_small'=> $image_small
                        );
                        
                    }
                    
                    $data['category_product'] = array();
                    
                    $categories_parent = $this->model_catalog_category->getCategories($this->request->get['id']);
                    
                        foreach($categories_parent as $parent) {
                            
                            
                            $filters = array(
                                'filter_category_id' => $parent['category_id'],
                                'start'              => 0,
                                'limit'              => $this->config->get('config_product_limit')
                            );
                            
                            $products_category = $this->model_catalog_product->getProducts($filters);
                            
                                $products_all = array();
                                
                                foreach($products_category as $result){
                                    
                                    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                                        $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                                    } else {
                                        $price = false;
                                    }
                        
                                    if ($result['image']) {
                                            $image_small = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                                        } else {
                                            $image_small = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                                    }
                                    
                                    
                                   $products_all[] = array(
                                       'product_id' => $result['product_id'],
                                        'name'       => $result['name'],
                                        'price'      => $price,
                                        'thumb'=> $image_small,
                                        'shipping'   => $result['shipping']
                                   ); 
                                   
                                }
                            
                            $data['category_product'][] = array(
                                'category_id' => $parent['category_id'],
                                'name'        => $parent['name'],
                                'products'    => $products_all
                            );
                                
                        }
                    
                    
                }
                
                
                
                
                if($this->request->get['check'] == 'default'){
                    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/category_popup.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/category_popup.tpl', $data));
                    } else {
                        $this->response->setOutput($this->load->view('default/template/common/category_popup.tpl', $data));
                    } 
                }elseif($this->request->get['check'] == 'alco'){
                    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/category_popup_alco.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/category_popup_alco.tpl', $data));
                    } else {
                        $this->response->setOutput($this->load->view('default/template/common/category_popup_alco.tpl', $data));
                    }
                }
 
            }
            
        }
        
        public function info(){
            
            $this->load->model('catalog/product');
            $this->load->model('tool/image');
            
            if ($this->request->server['REQUEST_METHOD'] == 'POST' || $this->request->server['REQUEST_METHOD'] == 'GET'){
                
                if(isset($this->request->get['id']) && $this->request->get['id']){
                  
                    $product = $this->model_catalog_product->getProduct($this->request->get['id']);
                    $data['name'] = $product['name'];
                    $data['description'] = html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8');
                    
                    if($product['image']){
                       $data['image'] = $this->model_tool_image->cropsize($product['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height')); 
                    }else{
                       $data['image'] = $this->model_tool_image->cropsize("placeholder.png", $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
                    }
                    
                    $data['shipping'] = $product['shipping'];
                    $data['minimum'] = $product['minimum'];
                    $data['product_id'] = $product['product_id'];
                    
                }
                
                
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/product_info.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/product_info.tpl', $data));
                    } else {
                        $this->response->setOutput($this->load->view('default/template/common/product_info.tpl', $data));
                } 
                
            }
            
        }
}

?>