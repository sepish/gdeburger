<?php
class ControllerCommonMenu extends Controller {
        public function index() {
            
            // Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');
                
                if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		$data['categories'] = array();
                
                $data['text_menu'] = $this->config->get('config_text_menu');

		$categories = $this->model_catalog_category->getCategories(0);
                
		foreach ($categories as $category) {
			if ( $category['top'] && ($this->config->get('config_customer_group_id') == $category['customer_group_id'] || $category['customer_group_id'] == '1') ) {
				// Level 2
				$children_data = array();
                                
                                if($category['image']){
                                    $image = 'image/' . $category['image'];
                                }else{
                                    $image = '';
                                }

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					$children_data[] = array(
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				$active_category = false;
				if ( isset($this->request->get['path']) ) {
					if ($parts_last == $category['category_id']) {
						$active_category = 'active';
					}

					if ( !empty($parts[0]) && $parts[0] == $category['category_id'] ) {
						$active_category = 'active';
					}
				}
                                
                                $check = false;
                                
                                if($category['top'] && !$category['bottom']){
                                   $check = 'default'; 
                                }elseif($category['top'] && $category['bottom']){
                                   $check = 'alco'; 
                                }
                                
                                
                                

				// Level 1
				$data['categories'][] = array(
                                        'category_id'   => $category['category_id'],
					'name'     	=> $category['name'],
                                        'image'         => $image,
                                        'check'         => $check,
					'active'  	=> $active_category,
					'children' 	=> $children_data,
					'column'   	=> $category['column'] ? $category['column'] : 1,
					'href'     	=> $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}
                
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/menu.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/menu.tpl', $data);
		} else {
			return $this->load->view('default/template/common/menu.tpl', $data);
		}
            
        }
}
?>