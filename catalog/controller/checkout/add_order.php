<?php
class ControllerCheckoutAddOrder extends Controller {
    
    public function index() {
        
        $this->load->language('checkout/checkout');
        $json = array();
        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
        
			if ((utf8_strlen($this->request->post['firstname']) < 2) || (utf8_strlen($this->request->post['firstname']) > 32)) {
				$json['error']['firstname'] = $this->language->get('error_firstname');
			}
        
			if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
				$json['error']['telephone'] = $this->language->get('error_telephone');
			}
        
			if ($this->request->post['shipping_method'] == '') {
				$json['error']['shipping_method'] = $this->language->get('error_shipping');
			}
        
			if(isset($this->request->post['delivery_street']) || isset($this->request->post['delivery_house'])){
				if(empty($this->request->post['delivery_street']) || empty($this->request->post['delivery_house'])){
					$json['error']['address'] = $this->language->get('error_address');
				}
			}
        
			if(isset($this->request->post['date']) && isset($this->request->post['time'])){
				if($this->request->post['date'] == '' && $this->request->post['time'] == ''){
					$json['error']['time'] = $this->language->get('error_time');
				}
			}
            
            
			if (!isset($json['error'])) {

				$order_data = array();

				$order_data['totals'] = array();
				$total = 0;
				$taxes = $this->cart->getTaxes();

				$this->load->model('extension/extension');

				$sort_order = array();

				$results = $this->model_extension_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);

				foreach ($results as $result) {
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('total/' . $result['code']);

						$this->{'model_total_' . $result['code']}->getTotal($order_data['totals'], $total, $taxes);
						}
					}

					$sort_order = array();

					foreach ($order_data['totals'] as $key => $value) {
						$sort_order[$key] = $value['sort_order'];
					}

					array_multisort($sort_order, SORT_ASC, $order_data['totals']);

					$order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
					$order_data['store_id'] = $this->config->get('config_store_id');
					$order_data['store_name'] = $this->config->get('config_name');

					if ($order_data['store_id']) {
						$order_data['store_url'] = $this->config->get('config_url');
					} else {
						$order_data['store_url'] = HTTP_SERVER;
					}

					$this->load->model('account/customer');
					$this->load->model('catalog/product');


					$order_data['customer_id'] = 0;
					$order_data['customer_group_id'] = 1;
					$order_data['firstname'] = $this->request->post['firstname'];
					$order_data['lastname'] = '';
					$order_data['email'] = (isset($this->request->post['email']) && $this->request->post['email']) ? $this->request->post['email'] : '';
					$order_data['telephone'] = $this->request->post['telephone'];
					$order_data['fax'] = '';
					$order_data['custom_field'] = '';


					$order_data['payment_firstname'] = $this->request->post['firstname'];
					$order_data['payment_lastname'] = '';
					$order_data['payment_company'] = '';
					$order_data['payment_address_1'] = '';
					$order_data['payment_address_2'] = '';
					$order_data['payment_city'] = '';
					$order_data['payment_postcode'] = '';
					$order_data['payment_zone'] = '';
					$order_data['payment_zone_id'] = '';
					$order_data['payment_country'] = '';
					$order_data['payment_country_id'] = '';
					$order_data['payment_address_format'] = '';
					$order_data['payment_custom_field'] = '';
					$order_data['payment_method'] = '';
					$order_data['payment_code'] = '';

					if(isset($this->request->post['delivery_street']) && $this->request->post['delivery_street']){
						$street = 'Улица: '.$this->request->post['delivery_street'];
					} else {
						$street = '';
					}
					if(isset($this->request->post['delivery_house']) && $this->request->post['delivery_house']){
						$house = 'Дом: '.$this->request->post['delivery_house'];
					} else {
						$house = '';
					}
					if(isset($this->request->post['delivery_housing']) && $this->request->post['delivery_housing']){
					   $housing = 'Корпус: '.$this->request->post['delivery_housing'];
					} else {
					   $housing = '';
					}
					if(isset($this->request->post['delivery_flat']) && $this->request->post['delivery_flat']){
					   $flat = 'Квартира: '.$this->request->post['delivery_flat'];
					} else {
					   $flat = '';
					}

					$html_address = $street.' '.$house.' '.$housing.' '.$flat;



					$order_data['shipping_firstname'] = $this->request->post['firstname'];
					$order_data['shipping_lastname'] = '';
					$order_data['shipping_company'] = '';
					$order_data['shipping_address_1'] = $html_address;
					$order_data['shipping_address_2'] = '';
					$order_data['shipping_city'] = '';
					$order_data['shipping_postcode'] = '';
					$order_data['shipping_zone'] = '';
					$order_data['shipping_zone_id'] = '';
					$order_data['shipping_country'] = '';
					$order_data['shipping_country_id'] = '';
					$order_data['shipping_address_format'] = '';
					$order_data['shipping_custom_field'] = array();
					$order_data['shipping_method'] = $this->request->post['shipping_method'];
					$order_data['shipping_code'] = $this->request->post['shipping_code'];

					$order_data['products'] = array();

					foreach ($this->cart->getProducts() as $product) {
						$option_data = array();
						if($product['option']){
							foreach ($product['option'] as $option) {
									$option_data[] = array(
											'product_option_id'       => $option['product_option_id'],
											'product_option_value_id' => $option['product_option_value_id'],
											'option_id'               => $option['option_id'],
											'option_value_id'         => $option['option_value_id'],
											'name'                    => $option['name'],
											'value'                   => $option['value'],
											'type'                    => $option['type']
									);
							}
						}

						$order_data['products'][] = array(
							'product_id' => $product['product_id'],
							'name'       => $product['name'],
							'model'      => $product['model'],
							'option'     => $option_data,
							'download'   => $product['download'],
							'quantity'   => $product['quantity'],
							'subtract'   => $product['subtract'],
							'price'      => $product['price'],
							'total'      => $product['total'],
							'tax'        => $this->tax->getTax($product['price'], $product['tax_class_id']),
							'reward'     => $product['reward']
						);
					}

					$order_data['vouchers'] = array();
					$order_data['comment'] = $this->request->post['comment'];
					$order_data['total'] = $total;
					if(isset($this->request->post['date']) && isset($this->request->post['time'])){
						$time_delivery = $this->request->post['date'].' - '.$this->request->post['time'];
					}else{
						$time_delivery = 'Как можно скорее';
					}
					$order_data['time_delivery'] = $time_delivery;
					$order_data['affiliate_id'] = 0;
					$order_data['commission'] = 0;
					$order_data['marketing_id'] = 0;
					$order_data['tracking'] = '';

					$order_data['language_id'] = $this->config->get('config_language_id');
					$order_data['currency_id'] = $this->currency->getId();
					$order_data['currency_code'] = $this->currency->getCode();
					$order_data['currency_value'] = $this->currency->getValue($this->currency->getCode());
					$order_data['ip'] = $this->request->server['REMOTE_ADDR'];

					if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
						$order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
					} elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
						$order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
					} else {
						$order_data['forwarded_ip'] = '';
					}

					if (isset($this->request->server['HTTP_USER_AGENT'])) {
						$order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
					} else {
						$order_data['user_agent'] = '';
					}

					if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
						$order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
					} else {
						$order_data['accept_language'] = '';
					}


					$this->load->model('checkout/order');

					$this->session->data['order_id'] = $this->model_checkout_order->addOrder($order_data);

					$json['success'] = $this->language->get('text_success');
					$this->load->controller('checkout/success');
					//$this->cart->clear();
			}
		}
       $this->response->setOutput(json_encode($json)); 
    }
	
	public function editShipping(){
		$json = array();
		if($this->request->post && !isset($this->request->post['shipping_refresh'])){
			    if(isset($this->session->data['shipping_method'])){
					unset($this->session->data['shipping_method']);
				}
				
			$this->session->data['shipping_method']['code'] = $this->request->post['shipping_code'];
			
			$this->session->data['shipping_method']['title'] = $this->request->post['shipping_method'];
			$this->load->model('shipping/' . $this->request->post['shipping_code']);
			$quote = $this->{'model_shipping_' . $this->request->post['shipping_code']}->getQuote('123');

			$this->session->data['shipping_method']['cost'] = false;
			
				if($this->session->data['shipping_method']){
					$json['success'] = true;
			}
		} else {
			if(isset($this->session->data['shipping_method'])){
					unset($this->session->data['shipping_method']);
			}
		}

		$this->response->setOutput(json_encode($json)); 
		
	}
	
	public function apiGoogleMaps(){
		$json = array();
		
		if($this->request->post){
			$this->session->data['shipping_method']['street'] = $this->request->post['street'];
			$this->session->data['shipping_method']['house'] = $this->request->post['house'];
			$destinations = str_replace(" ","+",'Харьков '.$this->request->post['street'].' '.$this->request->post['house']);
			$json['destinations'] = $destinations;
			if ($ch = @curl_init()) 
            { 
              // Устанавливаем URL запроса 
              @curl_setopt($ch, CURLOPT_URL, "https://maps.googleapis.com/maps/api/distancematrix/json?origins=49.989590,36.234852&destinations={$destinations}&language=ru&mode=driving"); 
              // При значении true CURL включает в вывод заголовки 
              @curl_setopt($ch, CURLOPT_HEADER, false); 
              // Куда помещать результат выполнения запроса: 
              //  false - в стандартный поток вывода, 
              //  true - в виде возвращаемого значения функции curl_exec. 
              @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
              // Максимальное время ожидания в секундах 
              @curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30); 
              
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              // Установим значение поля User-agent 
              // Выполнение запроса 
              $datas = @curl_exec($ch); 
              // Вывести полученные данные 
             
              // Особождение ресурса

              @curl_close($ch); 
            }
            
            $result2 = json_decode($datas,1);
			
			if(isset($result2['rows'][0]['elements'][0]['distance']['value'])){
			
				$km = $result2['rows'][0]['elements'][0]['distance']['value'];
				$json['km'] = $km;
				
				$cost_array = explode(",",$this->config->get('flat_cost'));
				
				if($km < 15000){
					$this->session->data['shipping_method']['cost'] = $cost_array[0];
				} elseif($km >= 15000 && $km < 20000){
					$this->session->data['shipping_method']['cost'] = $cost_array[1];
				} elseif($km >= 20000){
					$this->session->data['shipping_method']['cost'] = $cost_array[2];
				} 
				if($km && $this->session->data['shipping_method']['cost']){
					$json['success'] = true;
				}
			} else {
				$this->session->data['shipping_method']['cost'] = 50;
                $json['success'] = true;
			}
		}
		
		
		$this->response->setOutput(json_encode($json)); 
	}
	    
}
?>