<?php
class ControllerModuleHTML extends Controller {
	public function index($setting) {
		$data['module_id'] = substr($setting['name'], strrpos($setting['name'],"|")+1);

		$data['languagese'] = $this->language->getGroupData();

		if (isset($setting['module_description'][$this->config->get('config_language_id')])) {
			$data['heading_title'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');
			$data['html'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');

			if ( empty($setting['module_description'][$this->config->get('config_language_id')]['description_for_html']) ) {
				$data['description_for_html'] = '';
			}

			if ( !empty($setting['module_description'][$this->config->get('config_language_id')]['html_tpl']) ) {
				$data['html_tpl'] = $setting['module_description'][$this->config->get('config_language_id')]['html_tpl'];

				$description_for_html['heading_title'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');
				$description_for_html['text'] = $data['description_for_html'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['description_for_html'], ENT_QUOTES, 'UTF-8');
                                $description_for_html['address'] = $this->config->get('config_address');
                                $description_for_html['address2'] = $this->config->get('config_comment');
                                $description_for_html['telephone1'] = $this->config->get('config_telephone');
                                $description_for_html['telephone2'] = $this->config->get('config_telephone_2');
								$description_for_html['telephone3'] = $this->config->get('config_telephone_3');
                                $description_for_html['instagram'] = $this->config->get('config_instagram');
                                $description_for_html['email'] = $this->config->get('config_email');
								$description_for_html['email2'] = $this->config->get('config_skype');
                                $description_for_html['vk'] = $this->config->get('config_vk');
                                $description_for_html['background_seo'] = $this->config->get('config_image');
                                $description_for_html['fb'] = $this->config->get('config_facebook');
				$data['html_tpl'] = $this->load->view('default/template/information/html/' . $data['html_tpl'] . '.tpl', $description_for_html);
			} else {
				$data['description_for_html'] = '';
				$data['html_tpl'] = '';
			}

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/html.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/html.tpl', $data);
			} else {
				return $this->load->view('default/template/module/html.tpl', $data);
			}
		}
	}
}