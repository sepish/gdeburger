<?php
class ControllerNewsOurTeam extends Controller {
        public function index() {
            
            $this->load->model('tool/image');
            $this->load->model('catalog/news');
            
            $data['our_team'] = array();
            
            $our_team = $this->model_catalog_news->getAllNauthors();
                  
            foreach($our_team as $team){
                
                if($team['image']){
                    $image = $this->model_tool_image->resize($team['image'],360,540);
                }else{
                    $image = $this->model_tool_image->resize("placeholder.png",360,540);
                }
                
                $data['our_team'][] = array(
                    'name'  => $team['name'],
                    'image' => $image,
                    'ctitle'=> $team['ctitle']
                );
                
            }
            
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/news/our_team.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/news/our_team.tpl', $data);
		} else {
			return $this->load->view('default/template/news/our_team.tpl', $data);
            }
            
            
        }
        
        public function sendmail(){
            
            $json = array();
		$this->language->load('information/contact');
		//if ($this->request->server['REQUEST_METHOD'] == 'POST') {


			if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 32)) {
				$json['error']['name'] = $this->language->get('error_name');
			}
                        
                        if ((utf8_strlen($this->request->post['yearsold']) < 1) || (utf8_strlen($this->request->post['yearsold']) > 32)) {
				$json['error']['yearsold'] = $this->language->get('error_yearsold');
			}
                        
                        if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
				$json['error']['telephone'] = $this->language->get('error_telephone');
			}

			if ((utf8_strlen($this->request->post['message']) < 10) || (utf8_strlen($this->request->post['message']) > 3000)) {
				$json['error']['message'] = $this->language->get('error_enquiry');
			}
                        
                        if(empty($this->request->post['vacancy'])){
                                $json['error']['vacancy'] = $this->language->get('error_vacancy');
                        }
                        //var_dump($this->request->post);
                        if(empty($this->request->files['file']['name'])){
                                $json['error']['file'] = $this->language->get('error_file');
                        }else{
                            $filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));
                            
                            move_uploaded_file($this->request->files['file']['tmp_name'], DIR_DOWNLOAD . $filename);
                        }

			if (!isset($json['error'])) {
                                
                                $html = '';
                                $html .='Имя: '.$this->request->post['name']."\n\n";
                                $html .='Возраст: '.$this->request->post['yearsold']."\n\n";
                                $html .='Телефон: '.$this->request->post['telephone']."\n\n";
                                $html .='Сообщение: '.$this->request->post['message']."\n\n";
                                
				$mail = new Mail();
                                $mail->protocol = $this->config->get('config_mail_protocol');
                                $mail->parameter = $this->config->get('config_mail_parameter');
                                $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
                                $mail->smtp_username = $this->config->get('config_mail_smtp_username');
                                $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
                                $mail->smtp_port = $this->config->get('config_mail_smtp_port');
                                $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
                                
				$mail->setTo($this->config->get('config_email'));
				$mail->setFrom($this->config->get('config_email2'));
				$mail->setSender($this->request->post['name']);
                                if(is_file(DIR_DOWNLOAD.$filename)){
                                $mail->addAttachment(DIR_DOWNLOAD.$filename);
                                }
				$mail->setSubject(html_entity_decode(sprintf($this->language->get('email_subject'), $this->request->post['name']), ENT_QUOTES, 'UTF-8'));
                                $mail->setText(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));
				//$mail->setHtml();
				$mail->send();

				$json['success'] = $this->language->get('text_success');
			}
		//}

		$this->response->setOutput(json_encode($json));
            
            
            
            
            
        }
}
?>