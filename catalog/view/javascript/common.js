$(document).on('click', '.menu-pop-up_quantity a', function(event) {
  event.preventDefault();
  var nub_quantity = parseInt($(this).parent().find('#quantity').text());
  var quantity = parseInt($(this).parent().find('#quantity').text());
  if ($(this).is('.increase')) {
    quantity++;
    $(this).parent().find('#quantity').text(quantity);
  } else if ($(this).is('.decrease')) {
    if (nub_quantity != '1') {
      quantity--;
      $(this).parent().find('#quantity').text(quantity > 0 ? quantity : 0)
    }
  }
});
$(document).ready(function() {
  callme.init('#callme-button');

  $(document).on('submit','#order-form',function(e) {
    e.preventDefault();
    var data = $('#order-form').serialize();
    var self = $(this);

    $.ajax({
      url: 'index.php?route=checkout/add_order',
      type: 'post',
      data: data,
      dataType: 'json',
      beforeSend: function() {
        $('.checkout-pop_up').addClass('loading');
        self.find('button').prop('disabled',true);
      },
      complete: function() {
        $('.checkout-pop_up').removeClass('loading');
        self.find('button').prop('disabled',false);
      },
      success: function(json) {
        if(json['error']){

          if(json['error']['firstname']){
            $('.danger-firstname').remove();
            $('#order-form input[name=\'firstname\']').parent().prepend('<span class="danger-firstname danger">'+json['error']['firstname']+'</span>');
          }else{
            $('.danger-firstname').remove();
          }

          if(json['error']['telephone']){
            $('.danger-telephone').remove();
            $('#order-form input[name=telephone]').parent().prepend('<span class="danger-telephone danger">'+json['error']['telephone']+'</span>');
          }else{
            $('.danger-telephone').remove();
          }

          if(json['error']['shipping_method']){
            $('.danger-shipping_method').remove();
            $('#order-form select[name=shipping_method]').parent().prepend('<span class="danger-shipping_method danger">'+json['error']['shipping_method']+'</span>');
          }else{
            $('.danger-shipping_method').remove();
          }

          if(json['error']['address']){
            $('.danger-address').remove();
            $('#order-form #address .box-input:eq(0)').prepend('<span class="danger-address danger">'+json['error']['address']+'</span>');
          }else{
            $('.danger-address').remove();
          }

          if(json['error']['time']){
            $('.danger-time').remove();
            $('#order-form .choose_time').before().prepend('<span class="danger-time danger">'+json['error']['time']+'</span>');
          }else{
            $('.danger-time').remove();
          }

        }
        if(json['success']){
          $("#order-form *").hide();
          $(".checkout-pop_up strong").addClass('success').text(json['success']);
          $('#cart-total').load('index.php?route=common/cart/info');
          $('#modal-cart').load('index.php?route=common/cart_popup/info');
        }
      }

    });

  });

  var cart = {
    'add': function(product_id, quantity) {
      $.ajax({
        url: 'index.php?route=checkout/cart/add',
        type: 'post',
        data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
        dataType: 'json',
        success: function(json) {
          $('.alert, .text-danger').remove();
          if (json['redirect']) {
            location = json['redirect'];
          }
          if (json['success']) {
            $('#cart-total').addClass('active');
            $('#cart-total').load('index.php?route=common/cart/info');
            $('#modal-cart').load('index.php?route=common/cart_popup/info');
          }
        },
        complete: function() {}
      });
    },
    'update': function(key, quantity) {
      $.ajax({
        url: 'index.php?route=checkout/cart/refresh',
        type: 'post',
        data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
        dataType: 'json',
        success: function(json) {
          $('#cart-total').load('index.php?route=common/cart/info');

          $('#modal-cart .box-size-bag .test').load('index.php?route=common/cart_popup/info .test');
          $(".box-size-bag .total-price span").text(json['total']);
        }
      });
    },
    'remove': function(key) {
      $.ajax({
        url: 'index.php?route=checkout/cart/remove',
        type: 'post',
        data: 'key=' + key,
        dataType: 'json',
        success: function(json) {
          if (parseInt(json['total']) == 0) {
            $('#cart-total').removeClass('active');
            $('#modal-cart .box-btn_bag').hide();
          }
          $('#cart-total').load('index.php?route=common/cart/info');
          if (parseInt(json['total']) != 0) {
            $('#modal-cart .box-size-bag .test').load('index.php?route=common/cart_popup/info .test');
          } else {
            $('#modal-cart .box-size-bag').load('index.php?route=common/cart_popup/info .box-size-bag > ');
          }
          $(".box-size-bag .total-price span").text(json['total']);
          setTimeout(function() {
            jcf.replace('.box-size-bag .jcf-scrollable');
          }, 90);
        }
      });
    }
  }
  if ($(window).width() > 1025) {
    $(document).on('click', '.menu-pop-up_quantity button, .mob-info-menu .button, .menu-pop-up-alco .add_cart', function() {
      var id = $(this).data('id'),
        quantity = $(this).parent().find('#quantity').text();
      if (!quantity) {
        quantity = 1;
      }
      var element = $(this);
      var texts = $(this).text();
      $(this).text('Добавлено');
      if ($(window).width() > 767) {
        $(this).css('background', '#e13b3f');
      }
      cart.add(id, quantity);
      setTimeout(function() {
        element.text(texts);
      }, 2000);
    });
  }

  if ($(window).width() < 1025) {
    $(document).on('touchstart', '.menu-pop-up_quantity button, .mob-info-menu .button, .menu-pop-up-alco .add_cart', function() {
      var id = $(this).data('id'),
        quantity = $(this).parent().find('#quantity').text();
      if (!quantity) {
        quantity = 1;
      }
      var element = $(this);
      var texts = $(this).text();
      $(this).text('Добавлено');
      if ($(window).width() < 767) {
        $(this).css('background', '#000');
      }
      cart.add(id, quantity);
      setTimeout(function() {
        element.text(texts);
      }, 2000);
    });
  }
  $(document).on('click', '.bag-pop_up .bag-product .quantity a', function(event) {
    event.preventDefault();
    var nub_quantity = parseInt($(this).parent().find('.counter_quantity').text());
    var quantity = parseInt($(this).parent().find('.counter_quantity').text());
    var key = $(this).data('key');
    if ($(this).is('.increase')) {
      quantity++;
      $(this).parent().find('.counter_quantity').text(quantity);
    } else if ($(this).is('.decrease')) {
      if (nub_quantity != '1') {
        quantity--;
        $(this).parent().find('.counter_quantity').text(quantity > 0 ? quantity : 0)
      }
    }
    cart.update(key, quantity);
  });
  $(document).on('click', '.bag-pop_up .test .bag-product .delete', function() {
    var key = $(this).data('key');
    cart.remove(key);
  });
  $(document).on('click', '.content-menu div.col', function() {
    var id = $(this).data('id'),
      check = $(this).data('check');
    var o = $(".content-menu").offset().top + 50;
    $.ajax({
      url: '/index.php?route=common/category_popup&check=' + check + '&id=' + id,
      type: 'get',
      success: function(html) {
        $('body').prepend(html);
        var e = window.pageYOffset || document.documentElement.scrollTop;
        if (check == 'default') {
          $(".mask").show();
          $(".menu-pop-up .img-menu-pop-up").show();
          $(".menu-pop-up").addClass("show-box");
          $(".mob-pop-up_menu").addClass("show-box").css("top", e);
        } else if (check == 'alco') {
          $(".mask").show();
          $(".menu-pop-up-alco .img-menu-pop-up").show();
          $(".menu-pop-up-alco").addClass("show-box");
          $(".mob-pop-up_menu").addClass("show-box").css("top", e);
        }
        jcf.replaceAll();
      }
    });
  });
  $(document).on('click', '.news-slide ul li .info-news .btn-news', function() {
    var id = $(this).data('id');
    var a = $(".news-content").offset().top + 50
    $.ajax({
      url: '/index.php?route=news/article/info&id=' + id,
      type: 'get',
      success: function(html) {
        $('body').prepend(html);
        $(".new-pop-up").addClass("show-box").css("top", a)
        jcf.replaceAll();
      }
    });
  });
  $(document).on('click', '#modal-cart .issue_order-btn', function() {
    $.ajax({
      url: '/index.php?route=checkout/confirm/getFormAddOrder',
      type: 'get',
      success: function(html) {
        $('body').prepend(html);
        $.datepicker.setDefaults($.extend($.datepicker.regional.ru));
        $("#datepicker").datepicker({minDate: 0});
        $('body,html').scrollTop(0)
      }
    });
    return false;
  });
  $(document).on('change', '.checkout-pop_up select', function() {
    var code = $(this).find('option:selected').data('kode');
    if (code == 'flat.flat' || code == 'free.free') {
      $("#address").find('input[name=delivery_street]').removeAttr('disabled');

      $('#address').slideDown();
    } else {
      $("#address").find('input').each(function() {
        $(this).attr("disabled", "disabled");
        $(this).val('');
      })
      $('#address').slideUp();
    }
    $('#shipping_code').val(code);
    if (code) {
      var code_array = code.split('.');
      $.ajax({
        url: '/index.php?route=checkout/add_order/editShipping',
        type: 'post',
        dataType: 'json',
        data: 'shipping_code=' + code_array[0] + '&shipping_method=' + $("select[name='shipping_method'] option:selected").val(),
        success: function(json) {
          if (json['success']) {
            $('#order-form .total-price').load('index.php?route=checkout/confirm/getFormAddOrder .total-price >');
            $('#cart-total').load('index.php?route=common/cart/info');
            $('#modal-cart').load('index.php?route=common/cart_popup/info');
          }

          var delivery_street = $('input[name=delivery_street]').val();
          var delivery_house = $('input[name=delivery_house]').val();
          if (delivery_street && delivery_house) {
            var street = delivery_street;
            var house = delivery_house;
            $.ajax({
              url: '/index.php?route=checkout/add_order/apiGoogleMaps',
              type: 'post',
              dataType: 'json',
              data: 'street=' + street + '&house=' + house,
              success: function(json) {
                if (json['success']) {
                  $('#order-form .total-price').load('index.php?route=checkout/confirm/getFormAddOrder .total-price >');
                  $('#cart-total').load('index.php?route=common/cart/info');
                  $('#modal-cart').load('index.php?route=common/cart_popup/info');
                }
              }
            });
          }

        }
      });
    } else {
      $.ajax({
        url: '/index.php?route=checkout/add_order/editShipping',
        type: 'post',
        dataType: 'json',
        data: 'shipping_refresh=1',
        success: function(json) {
          $('#order-form .total-price').load('index.php?route=checkout/confirm/getFormAddOrder .total-price >');
          $('#cart-total').load('index.php?route=common/cart/info');
          $('#modal-cart').load('index.php?route=common/cart_popup/info');
        }
      });
    }
  });
  $(document).on('blur', '.google_api', function() {
    var delivery_street = $('input[name=delivery_street]').val();
    var delivery_house = $('input[name=delivery_house]').val();
    if (delivery_street && delivery_house) {
      var street = delivery_street;
      var house = delivery_house;
      $.ajax({
        url: '/index.php?route=checkout/add_order/apiGoogleMaps',
        type: 'post',
        dataType: 'json',
        data: 'street=' + street + '&house=' + house,
        success: function(json) {
          if (json['success']) {
            $('#order-form .total-price').load('index.php?route=checkout/confirm/getFormAddOrder .total-price >');
            $('#cart-total').load('index.php?route=common/cart/info');
            $('#modal-cart').load('index.php?route=common/cart_popup/info');
          }
        }
      });
    }
  })
  $(document).on('click', '.menu-pop-up ul li a', function() {
    var id = $(this).data('id');
    $(this).parent().parent().find('li a span.active').removeClass('active');
    $(this).parent().parent().find('li a.active').removeClass('active');
    $(this).find('.nema-product').addClass('active');
    $(this).addClass('active');
    $.ajax({
      url: '/index.php?route=common/category_popup/info&id=' + id,
      type: 'get',
      success: function(html) {
        $('.menu-pop-up .img-menu-pop-up').html(html);
      }
    });
  });
  $(document).on('click', '.pagination-product .prev, .pagination-product .next', function() {
    if (!$('.block-menu ul li a span.active').length) {
      $('.block-menu ul li a').eq(0).trigger('click');
    } else if ($('.block-menu ul li a.active').length) {
      if ($(this).is('.next')) {
        var indexes;
        $('.block-menu ul li').find('a.active').each(function() {
          indexes = $(this).index('a');
        })
        $('.block-menu ul li a').eq(indexes + 1).trigger('click');
      } else if ($(this).is('.prev')) {
        var indexes;
        $('.block-menu ul li').find('a.active').each(function() {
          indexes = $(this).index('a');
        })
        $('.block-menu ul li a').eq(indexes - 1).trigger('click');
      }
      $('.pagination-product').siblings('.jcf-scrollable-wrapper').find('.jcf-scrollable').scrollTop($('.block-menu ul li').find('a.active').parents('li').position().top)
    }
  });
  $(document).on('click', '#delivery', function() {
    $.ajax({
      url: '/index.php?route=information/contact/delivery',
      type: 'get',
      success: function(html) {
        $('body').prepend(html);
        $('.delivers').fadeIn(1);
        $('.mask').show();
      }
    });
    $(document).on('click', '.delivers .checkout-close', function() {
      $(this).parent().remove();
    });
  });
});
