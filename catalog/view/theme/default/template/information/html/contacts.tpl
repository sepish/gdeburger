<div class="map-content">
          <div class="content-size-seo">
            <div class="address-box">
              <span class="title-content"><?php echo $heading_title;?></span>
              <address><?php echo html_entity_decode($address);?> <span><?php echo $address2;?></span></address>
              <ul>
			  <li><span style="color: #fff;font-weight: 700;display: block;line-height: 22px;font-size: 16px;">Ресторан</span></li>
                  <li><a href="tel:<?php echo $telephone1;?>"><?php echo $telephone1;?></a>
                </li>
                <li><a href="tel:<?php echo $telephone2;?>"><?php echo $telephone2;?></a>
                </li>
              </ul>
				  <span style="color: #fff;padding-left: 40px;font-weight: 700;display: block;line-height: 22px;font-size: 16px;">Контроль качества</span>
				  <a class="link-address" href="mailto:<?php echo $email;?>"><?php echo $email;?></a>
				  <span style="color: #fff;padding-left: 40px;font-weight: 700;display: block;line-height: 22px;font-size: 16px;">Служба доставки</span>
			  <ul style="margin-bottom: 0px;">
                  <li><a href="tel:<?php echo $telephone3;?>"><?php echo $telephone3;?></a>
                </li>
              </ul>
			  <a class="link-address" href="mailto:<?php echo $email2;?>" style="margin-top: 7px;"><?php echo $email2;?></a>
              <div class="soc-seti">
                <span>Следите за нами:</span>
                <a target="_blank" class="instagram" href="<?php echo $instagram;?>"></a>
                <a target="_blank"  class="vk" href="<?php echo $vk;?>"></a>
                <a  target="_blank" class="facebook" href="<?php echo $fb;?>"></a>
              </div>
              <span class="btn-callback">Написать нам</span>
            </div>
          </div>
          <div id="map_canvas"></div>
</div>
