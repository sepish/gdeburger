<div class="checkout-pop_up">
      <span class="checkout-close"></span>
      <span class="title-pop-up">Оформление заказа</span>
      <strong>Для оформления заполните поля и наш менеджер свяжется с Вами для уточнения заказа</strong>
      <form id="order-form">
        <div class="box-input">
          <input type="text" name="firstname" id="input-name" value="">
          <label for="input" class="ph-color" id="inputLbl">Имя<sup>*</sup>
          </label>
        </div>
        <div class="box-input">
          <input type="text" name="telephone" id="input-tel" value="">
          <label for="input" class="ph-color" id="inputLb2">Телефон<sup>*</sup>
          </label>
        </div>
        <div class="box-input">
          <input type="email" name="email" value="" placeholder="E-mail">
        </div>
        <div class="box-input">
          <select name="shipping_method" data-jcf='{"wrapNative": false, "wrapNativeOnMobile": false, "fakeDropInBody": false, "useCustomScroll": false}'>
            <option value="">-- Выберите способ доставки --</option>
            <?php foreach($shipping_methods as $method) { ?>
				<?php if($method['selected']) { ?>
					<option data-kode="<?php echo $method['code2'];?>" value="<?php echo $method['title'];?>" selected="selected">-- <?php echo $method['title'];?> --</option>
				<?php } else { ?>
					<option data-kode="<?php echo $method['code2'];?>" value="<?php echo $method['title'];?>">-- <?php echo $method['title'];?> --</option>
				<?php }?>
            <?php }?>
          </select>
            <input type="hidden" id="shipping_code" name="shipping_code" value="<?php echo $shipping_methods_code;?>"/>
        </div>
        <div id="address" <?php if($shipping_methods_code == 'flat' || $shipping_methods_code == 'free') { ?>style="display:block;"<?php } else { ?>style="display:none;"<?php }?>>
        <div class="box-input">
          <input type="text" name="delivery_street" value="<?php echo $street;?>" id="route" placeholder="Улица" <?php if($shipping_methods_code != 'flat' && $shipping_methods_code != 'free') { ?>disabled="disabled"<?php }?> class="google_api">
        </div>
        <div class="small-input">
          <div class="box-input">
            <input type="text" name="delivery_house" value="<?php echo $house;?>" id="street_number" placeholder="№ дома" <?php if($shipping_methods_code != 'flat' && $shipping_methods_code != 'free') { ?>disabled="disabled"<?php }?> class="google_api">
          </div>
          <div class="box-input">
            <input type="text" name="delivery_housing" value="" placeholder="Корпус" <?php if($shipping_methods_code != 'flat' && $shipping_methods_code != 'free') { ?>disabled="disabled"<?php }?>>
          </div>
          <div class="box-input">
            <input type="text" name="delivery_flat" value="" placeholder="Квартира" <?php if($shipping_methods_code != 'flat' && $shipping_methods_code != 'free') { ?>disabled="disabled"<?php }?>>
          </div>
        </div>
        </div>
        <div class="choose_time" style="display:none;">
          <div class="calendar">
            <input type="text" name="date" id="datepicker" disabled="disabled" placeholder="Выберите дату">
          </div>
          <b>В</b>
          <div class="box-input">
            <input type="text" name="time" id="datetime" value="" placeholder="Время" disabled="disabled">
          </div>
        </div>
        <div class="checked-box">
          <label for="rather" class="rather">
            <input type="radio" name="delivery_attr" value="" id="rather" checked="checked" data-refresh="5" class="styled" value="Как можно скорее"/>Как можно скорее
          </label>
          <label for="choose_date" class="choose_date">
            <input type="radio" name="delivery_attr" value="" id="choose_date" data-refresh="5" class="styled" />Подобрать дату и время
          </label>
        </div>
        <div class="box-input textarea-box">
          <textarea placeholder="Комментарий к заказу" name="comment"></textarea>
        </div>
        <div class="box-btn_bag">
          <span class="total-price">
              <?php foreach($totals as $total) { ?>
				<?php if($total['code'] == 'shipping') { ?>
					<p>Доставка:<span><?php echo $total['text'];?></span></p>
				<?php } else { ?>
				    <em style="font-style: normal;"><?php echo $total['title'];?>:</em><span><?php echo $total['text'];?></span>
				<?php }?>

              <?php }?>
          </span>
          <button class="issue_order-btn" type="submit">Подтвердить заказ</button>
        </div>
      </form>
    </div>
<style>
    .danger{color:red;}
    strong.success{color: brown;}
</style>
<style>
    .loading {
        background-color: #eee;
        background-image: url('/image/loading.gif');
        background-position: center;
        background-repeat: no-repeat;
        opacity: 1;
        pointer-events: none;
    }
    .loading * {
        opacity: .8;
    }
</style>
<script>
jQuery(function($){
   $("#input-tel").mask("+380(99)9999999", {autoclear: false});
   if ($(window).width() < 767) {
     $("#input-tel").mask("+380999999999", {autoclear: false});
   } else {
     $("#input-tel").mask("+380(99)9999999", {autoclear: false});
   }
});
</script>
<script>

    $(document).on('change','.checked-box label',function(){

        if($(this).is('.rather')){
            $('.choose_time').slideUp();
            $('#datepicker').attr("disabled","disabled");
            $('#datetime').attr("disabled","disabled");
        }else if($(this).is('.choose_date')){
            $('.choose_time').slideDown();
            $('#datepicker').removeAttr('disabled');
            $('#datetime').removeAttr('disabled');
        }
    });

</script>
			<script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

$("#autocomplete").on('focus', function () {
    geolocate();
});

var placeSearch, autocomplete;
var componentForm = {
    street_number: 'short_name',
    route: 'long_name'
};

function initializec() {
    // Create the autocomplete object, restricting the search
    // to geographical location types.
	var defaultBounds = new google.maps.LatLngBounds(
	  new google.maps.LatLng(49.994507, 36.1457421));

    autocomplete = new google.maps.places.Autocomplete(
    /** @type {HTMLInputElement} */ (document.getElementById('route')), {
        types: ['geocode'],
		bounds: defaultBounds,
		componentRestrictions: {country: 'ua'}
    });
    // When the user selects an address from the dropdown,
    // populate the address fields in the form.
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        fillInAddress();
		google_api();
    });
}

// [START region_fillform]
function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();

    /*document.getElementById("latitude").value = place.geometry.location.lat();
    document.getElementById("longitude").value = place.geometry.location.lng();*/

    for (var component in componentForm) {
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
        }
    }

	$("#address").find('input').each(function() {
        $(this).removeAttr('disabled');
    })
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var geolocation = new google.maps.LatLng(
            position.coords.latitude, position.coords.longitude);

            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            document.getElementById("latitude").value = latitude;
            document.getElementById("longitude").value = longitude;

            autocomplete.setBounds(new google.maps.LatLngBounds(geolocation, geolocation));
        });
    }

}

function google_api() {
    var delivery_street = $('input[name=delivery_street]').val();
    var delivery_house = $('input[name=delivery_house]').val();
    if (delivery_street && delivery_house) {
      var street = delivery_street;
      var house = delivery_house;
      $.ajax({
        url: '/index.php?route=checkout/add_order/apiGoogleMaps',
        type: 'post',
        dataType: 'json',
        data: 'street=' + street + '&house=' + house,
        success: function(json) {
          if (json['success']) {
            $('#order-form .total-price').load('index.php?route=checkout/confirm/getFormAddOrder .total-price >');
            $('#cart-total').load('index.php?route=common/cart/info');
            $('#modal-cart').load('index.php?route=common/cart_popup/info');
          }
        }
      });
    }
  }

initializec();
// [END region_geolocation]
</script>
