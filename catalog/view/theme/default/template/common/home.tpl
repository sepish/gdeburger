<?php echo $header; ?>
<div id="main" class="clearfix">
        <div class="img-general cd-background-wrapper">
          <img src="<?php echo $background;?>" alt="<?php echo $title;?>">
          <div class="parallax-wrap cd-floating-background">
            <div class="parallax-box bg-img-parallax"><img src="<?php echo $background;?>" alt="<?php echo $title;?>"></div>
            <div class="logo parallax-box"><a href="<?php echo $home;?>"></a></div>
            <div class="btn-scroll"><span>Перейти к Меню<i></i></span></div>
          </div>
        </div>
    <div class="reservations-content">
          <div class="common-title">Резерв</div>
          <div class="reservations-box">
            <a href="http://letsbar.com.ua/place/gdeburger">
              <img src="/image/catalog/project_image/img1.png" alt="reservations">
            </a>
            <div class="text-reservations">
              <p><?php echo $text_reserve;?></p>
            </div>
            <div class="reservations-link">
              <a href="http://letsbar.com.ua/place/gdeburger">Забронировать стол</a>
            </div>
          </div>
        </div>
    <?php echo $menu;?>
  <?php echo $column_left; ?>
  <?php echo $content_top; ?>
  <?php echo $instagramm;?>
  <?php echo $content_bottom; ?>
  <?php echo $our_teams;?>
  <?php echo $column_right; ?>
</div>
<?php echo $footer; ?>
