<div class="menu-pop-up-alco">
      <span class="close-menu"></span>
      <div class="list-menu">
        <span class="title-menu"><?php echo $category_info['name'];?></span>
        <div class="block-menu">
          <?php if($category_product) { ?>
            <div class="jcf-scrollable">
            <?php foreach($category_product as $result) { ?>
            <span class="list-title"><?php echo $result['name'];?></span>
                <?php if($result['products']) { ?>
                    <ul class="ul-menu">
                        <?php foreach($result['products'] as $product) { ?>
                      <li>
                          <a>
                              <span class="nema-product"><?php echo $product['name'];?></span>
                              <span <?php if($product['shipping'] != '1') { ?>style="visibility:hidden;"<?php }?> class="add_cart" data-id="<?php echo $product['product_id'];?>">В корзину</span>
                              <span class="price"><?php echo $product['price'];?></span>
                          </a>
                      </li>
                        <?php }?>
                    </ul>
                <?php }?>
            <?php }?>
          </div>
          <div class="pagination-product">
            <span class="prev"></span>
            <span class="next"></span>
          </div>
          <?php } else { ?>
          <p><?php echo $text_empty;?></p>
          <?php }?>
        </div>
      </div>
      <div class="img-menu-pop-up">
        <img src="<?php echo $image_category;?>" alt="<?php echo $category_info['name'];?>" width="835" height="544">
      </div>
</div>

<div class="mob-pop-up_menu">
      <span class="title-pop-up"><?php echo $category_info['name'];?></span>
      <span class="checkout-close">close</span>
      <?php if($category_product) { ?>
      <ul>
          <?php foreach($category_product as $result) { ?>
		  <li class="category-li"><?php echo $result['name'];?></li>
            <?php if($result['products']) { ?>
                 <?php foreach($result['products'] as $product) { ?>
                    <li>
                        <img src="<?php echo $product['thumb'];?>" alt="<?php echo $product['name'];?>" width="110" height="62">
                      <div class="mob-info-menu">
                        <span class="name-product"><?php echo $product['name'];?></span>
                        <div>
                          <span class="price"><?php echo $product['price'];?></span>
                          <span <?php if($product['shipping'] != '1') { ?>style="visibility:hidden;"<?php }?> class="btn-mob-menu button" data-id="<?php echo $product['product_id'];?>">Добавить</span>
                        </div>
                      </div>
                    </li>
                <?php }?>
            <?php }?>
          <?php }?>
      </ul>
      <?php } else { ?>
          <p><?php echo $text_empty;?></p>
      <?php }?>
    </div>
