<div id="modal-cart">
<?php echo $cart_popup;?>
</div>

<div class="team-pop-up">
      <span class="checkout-close"></span>
      <span class="title-pop-up">Хочешь стать одним из нас?</span>
      <strong>Для этого выбери вакансию и заполни поля ниже</strong>
      <form action="javascript:void(null);" onsubmit="vacancysend()" id="form-vacancy">
        <div class="box-input vacancy">
          <select name="vacancy" data-jcf='{"wrapNative": false, "wrapNativeOnMobile": false, "fakeDropInBody": false, "useCustomScroll": false}'>
            <?php if(isset($vacancy) && $vacancy) { ?>
            <option value="">-- Выбери вакансию --</option>
                <?php foreach($vacancy as $vac) { ?>
                <option value="<?php echo $vac['value'];?>">-- <?php echo $vac['value'];?> --</option>
                <?php }?>
            <?php } else { ?>
            <option value="">-- Нет свободных вакансий --</option>
            <?php }?>
          </select>
        </div>
        <div class="box-input name">
          <input type="text" name="name" id="team-input" value="">
          <label for="team-input" class="ph-color" id="team-inputLb1">Как тебя зовут?<sup>*</sup>
          </label>
        </div>
        <div class="box-input yearsold">
          <input type="text" name="yearsold" id="team-input2" value="">
          <label for="team-input2" class="ph-color" id="team-inputLb2">Сколько тебе лет?<sup>*</sup>
          </label>
        </div>
        <div class="box-input telephone">
          <input type="tel" name="telephone" id="team-input3" value="">
          <label for="team-input3" class="ph-color" id="team-inputLb3">Твой номер телефона?<sup>*</sup>
          </label>
        </div>
        <div class="box-input team-box-textarea message">
          <textarea id="textarea-label" name="message"></textarea>
          <label for="team-input3" class="ph-color" id="team-inputLb4">Напиши нам о себе<sup>*</sup>
          </label>
        </div>
        <div class="fileform">
          <input id="upload" type="file" name="file" value=""/>
        </div>
        <span class="fileform-text">*Резюме без фотографии не рассматривается</span>
        <div class="box-btn-input">
          <input type="submit" name="button" value="Жду звонка">
        </div>
      </form>
    </div>

<div class="popup-write">
      <span class="checkout-close"></span>
      <span class="title-pop-up">Написать</span>
      <form action="javascript:void(null);" onsubmit="messagesend()" id="send-message">
        <div class="box-input">
          <input type="text" name="name" value="" placeholder="Имя">
        </div>
        <div class="box-input">
          <input type="email" name="email" value="" placeholder="E-mail">
        </div>
        <div class="box-input textarea-box">
          <textarea placeholder="Писать здесь" name="enquiry"></textarea>
        </div>
        <div class="box-btn-input">
          <input type="submit" name="button" value="Отправить">
        </div>
      </form>
    </div>  
      <div class="mask-window">
        <img src="image/catalog/project_image/orientation.jpg" alt="orientation">
    </div>          
<style>
    .danger{color:red;}
    span.success{text-align: center;padding: 10px;line-height: 19px;color: brown;}
</style>
            
<script>
    function vacancysend(){
        
        var fd = new FormData($("#form-vacancy")[0]);
                        $.ajax({
				url: 'index.php?route=news/our_team/sendmail',
				type: 'post',
                                processData: false,
                                contentType: false,
				data: fd,
                                dataType: 'json',
				success: function(json) {
                                   
                                   console.log(json);
                                                                     
                                    if(json['error']){
                                        if(json['error']['vacancy']){
                                            $('.danger-vacancy').remove();
                                            $("#form-vacancy .vacancy").before().prepend('<span class="danger-vacancy danger">'+json['error']['vacancy']+'</span>');
                                        }else{
                                            $('.danger-vacancy').remove();
                                        }
                                        
                                        if(json['error']['name']){
                                            $('.danger-name').remove();
                                            $("#form-vacancy .name").before().prepend('<span class="danger-name danger">'+json['error']['name']+'</span>');
                                        }else{
                                            $('.danger-name').remove();
                                        }
                                        
                                        if(json['error']['telephone']){
                                            $('.danger-telephone').remove();
                                            $("#form-vacancy .telephone").before().prepend('<span class="danger-telephone danger">'+json['error']['telephone']+'</span>');
                                        }else{
                                            $('.danger-telephone').remove();
                                        }
                                        
                                        if(json['error']['yearsold']){
                                            $('.danger-yearsold').remove();
                                            $("#form-vacancy .yearsold").before().prepend('<span class="danger-yearsold danger">'+json['error']['yearsold']+'</span>');
                                        }else{
                                            $('.danger-yearsold').remove();
                                        }
                                        
                                        if(json['error']['message']){
                                            $('.danger-message').remove();
                                            $("#form-vacancy .message").before().prepend('<span class="danger-message danger">'+json['error']['message']+'</span>');
                                        }else{
                                            $('.danger-message').remove();
                                        }
                                        
                                        if(json['error']['file']){
                                            $("#form-vacancy .fileform").css("border","1px solid red");
                                        }else{
                                            $("#form-vacancy .fileform").css("border","none");
                                        }
                                        
                                        
                                    }
                                    
                                    if(json['success']){
                                        $("#form-vacancy .box-btn-input").append('<span class="success">'+json['success']+'</span>');
                                        $('.danger-vacancy').remove();
                                        $('.danger-name').remove();
                                        $('.danger-telephone').remove();
                                        $('.danger-yearsold').remove();
                                        $('.danger-message').remove();
                                        setTimeout(function(){
                                            location.reload();
                                        },1200);
                                        
                                    }
                                }
                                
                        });
    }
    
    function messagesend(){
            var data = $('#send-message').serialize();
            $.ajax({
				url: 'index.php?route=information/contact/validatecontact',
				type: 'post',
				dataType: 'json',
				data: data,
				success: function(data) {

					if (data['error']) {
						if(data['error']['name']){
							$('span.error_name').remove();
							$('#send-message input[name="name"]').before('<span class="error_name danger">'+data['error']['name']+'</span>');
						}else{
							$('span.error_name').remove();
						}
						if(data['error']['email']){
							$('span.error_email').remove();
							$('#send-message input[name="email"]').before('<span class="error_email danger">'+data['error']['email']+'</span>');
						}else{
							$('span.error_email').remove();
						}

						if(data['error']['enquiry']){
							$('span.error_enquiry').remove();
							$('#send-message textarea[name="enquiry"]').before('<span class="error_enquiry danger">'+data['error']['enquiry']+'</span>');
						}else{
							$('span.error_enquiry').remove();
						}
					return false;
					}

					if (data['success']) {
						$('#send-message .box-btn-input').append('<span class="success">'+data['success']+'</span>');
						$('#send-message input[name=\'name\']').val('');
						$('#send-message input[name=\'email\']').val('');
						$('#send-message textarea[name=\'enquiry\']').val('');

						$('span.error_name').remove();
						$('span.error_email').remove();
						$('span.error_enquiry').remove();

						setTimeout(function(){
                                                    location.reload();
                                                },1200);
					}
				}
			});
    
    
    }
                
</script>
