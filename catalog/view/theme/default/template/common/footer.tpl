<?php echo $content_footer; ?>
<footer id="footer">
        <div class="footer-size">
          <span class="copyright"><?php echo $powered;?></span>
          <?php echo $developers;?>
        </div>
</footer>
</div>
<?php echo $modals ?>
<p id="topon"><a href="#top"><span></span></a></p>
<script src="catalog/view/javascript/mf/jquery-ui.min.js" type="text/javascript"></script>
<script src="catalog/view/js/vendor/jquery.flipster.js"></script>
<script src="catalog/view/js/vendor/jquery.mousewheel.js"></script>
<script src="catalog/view/js/vendor/masonry.pkgd.js"></script>
<script src="catalog/view/js/vendor/jcf.js"></script>
<script src="catalog/view/js/vendor/jcf.select.js"></script>
<script src="catalog/view/js/vendor/jcf-radio.js"></script>
<script src="catalog/view/js/vendor/sly.min.js"></script>
<script src="catalog/view/js/vendor/jcf.file.js"></script>
<script src="catalog/view/js/vendor/parallax-logo.js"></script>
<script src="catalog/view/js/vendor/jcf.scrollable.js"></script>
<script src="catalog/view/js/vendor/jquery.mask.min.js"></script>
<script src="catalog/view/js/vendor/jquery_action.js"></script>
<script src="catalog/view/js/vendor/jquery.maskedinput/dist/jquery.maskedinput.js"></script>
<script src="catalog/view/js/app.js"></script>
<script src="catalog/view/javascript/callme.js"></script>
<script src="catalog/view/javascript/common.js"></script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAheg6HIJVqnwpHbSq_Zoa-RPfAMitBT2g&libraries=places"></script>


    <script>
      function initialize(){
        var latlng = new google.maps.LatLng(49.989589, 36.234874);
        if($(window).width() > 767){
        var mapcanter =new google.maps.LatLng(49.989589, 36.234874);
         } else{
         var mapcanter =new google.maps.LatLng(49.989848, 36.233767);
        }
        var myOptions ={
         zoom: 16,
         center: mapcanter,
         scrollwheel: false,
         mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        // var image = '/image/catalog/project_image/marker.png';

        var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        var myMarker = new google.maps.Marker(
         {
          position: latlng,
          map: map,
          // icon: image,
          animation: google.maps.Animation.DROP
         }

        );
        google.maps.event.addListener(myMarker, 'click', toggleBounce);
        function toggleBounce() {

          if (myMarker.getAnimation() != null) {
            myMarker.setAnimation(null);
          } else {

          }
        }
        var infowindow = new google.maps.InfoWindow();
       }
	      setTimeout(function(){
			 google.maps.event.addDomListener(window, 'load', initialize);
		  },200);


    </script>

</body>
</html>
