<div class="menu-pop-up">
      <span class="close-menu"></span>
      <div class="list-menu">
        <span class="title-menu"><?php echo $category_info['name'];?></span>
        <div class="block-menu">
          <div class="jcf-scrollable">
            <?php if($products) { ?>
              <ul>
                  <?php foreach($products as $product) { ?>
                  <li><a data-id="<?php echo $product['product_id'];?>"><span class="nema-product"><?php echo $product['name'];?></span><span class="price"><?php echo $product['price'];?></span></a></li>
                  <?php }?>
            </ul>
            <?php } else { ?>
            <p><?php echo $text_empty;?></p>
            <?php }?>
          </div>
            <?php if($products) { ?>
                <div class="pagination-product">
                  <span class="prev"></span>
                  <span class="next"></span>
                </div>
            <?php }?>
        </div>
      </div>
      <div class="img-menu-pop-up">
          <img src="<?php echo $image_category;?>" alt="<?php echo $category_info['name'];?>" width="975" height="545">
      </div>
</div>

<div class="mob-pop-up_menu">
      <span class="title-pop-up"><?php echo $category_info['name'];?></span>
      <span class="checkout-close">close</span>
      <?php if($products) { ?>
      <ul>
          <?php foreach($products as $product) { ?>
        <li>
            <img src="<?php echo $product['thumb_small'];?>" alt="<?php echo $product['name'];?>" width="110" height="62">
          <div class="mob-info-menu">
            <span class="name-product"><?php echo $product['name'];?></span>
			<?php if($product['description']) { ?>
				<div class="name-text">
					<?php echo $product['description'];?>
				</div>
			<?php }?>
            <div>
              <span class="price"><?php echo $product['price'];?></span>
              <span <?php if($product['shipping'] != '1') { ?>style="visibility:hidden;"<?php }?> class="btn-mob-menu button" data-id="<?php echo $product['product_id'];?>">Добавить</span>
            </div>
          </div>
        </li>
        <?php }?>
      </ul>
      <?php } else { ?>
      <p><?php echo $text_empty;?></p>
      <?php }?>
</div>
