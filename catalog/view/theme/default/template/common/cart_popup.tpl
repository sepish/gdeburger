<div class="bag-pop_up">
      <div class="top-box-bag">
        <span class="title-bg_pop-up">Корзина:</span>
        <span class="close"></span>
      </div>
      <div class="box-size-bag">
          <?php if(isset($products) && $products) { ?>
            <script type="text/javascript">
            if ($(window).width() < 767) {
              $('.cart-product-box').removeClass('jcf-scrollable');
            };
             $(".top-box-bag .close").click(function() {
              $(".bag-pop_up").removeClass("show-bag"), $(".mask").hide(), $(".wrap,#header").removeClass("show-wrap"), $(".cart-bag").removeClass("active")
            });
            </script>
        <div class="cart-product-box jcf-scrollable">
            <div class="test">
          <?php foreach ($products as $product) { ?>
            <div class="bag-product">
            <span class="delete" data-key="<?php echo $product['key']; ?>"></span>
            <?php if ($product['thumb']) { ?>
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
            <?php }?>
            <span class="name-bag"><?php echo $product['name']; ?></span>
            <span class="price"><?php echo $product['price']; ?></span>
            <div class="quantity">
              <a class="counter_odd decrease" data-key="<?php echo $product['key']; ?>">prev</a>
              <span class="counter_quantity"><?php echo $product['quantity']; ?></span>
              <a class="counter_add increase" data-key="<?php echo $product['key']; ?>">next</a>
            </div>
            <span class="big-price"><?php echo $product['total']; ?></span>
          </div>
          <span class="line-figure"></span>
          <?php }?>
          <?php if(!isset($products) && !$products) { ?>
          <?php echo $text_empty; ?>
          <?php }?>
            </div>
        </div>
        <div class="box-btn_bag">
            <?php foreach ($totals as $total) { ?>
            <span class="total-price"><?php echo $total['title']; ?>:<span><?php echo $total['text']; ?></span></span>
            <?php }?>
          <span class="issue_order-btn"><?php echo $text_checkout; ?></span>
        </div>
          <?php }?>
          <?php if(empty($products)) { ?>
          <?php echo @$text_empty; ?>
          <?php }?>
      </div>
    </div>
