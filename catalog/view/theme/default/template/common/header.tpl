<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
  <meta charset="UTF-8" />
  <meta name="format-detection" content="telephone=no">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <title><?php echo $title; ?></title>
  <base href="<?php echo $base; ?>" />
  <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,700,500&subset=cyrillic,latin' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic,cyrillic-ext' rel='stylesheet' type='text/css'>
  <link rel="shortcut icon" href="image/catalog/project_image/favicon.ico" type="image/x-icon">
<?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
   <meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php if ($icon) { ?>
  <link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
  <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
  <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
  
  <link href="catalog/view/theme/default/stylesheet/index.css" rel="stylesheet">




  <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
  <?php } ?>


  <script>
    var LANGS = {};
    <?php $arr = $languagese; foreach($arr as $group => $langs){ ?>LANGS['<?php echo $group?>']={};<?php foreach($langs as $name_key => $value){?>LANGS['<?php echo $group?>']['<?php echo $name_key ;?>']='<?php echo $value ;?>';<?php } ?><?php } ?>
  </script>
<?php foreach ($scripts as $script) { ?>
  <script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
  <?php echo $google_analytics; ?>
</head>

<body class="<?php echo $class; ?>">
<div class="mask"></div>
<div class="wrap">
  <?php echo $content_header; ?>
  <header id="header">
        <div class="header-content">
          <nav>
            <div class="mob-btn-menu">
              <span id="bar-line1" class="menu-line"></span>
              <span id="bar-line2" class="menu-line"></span>
              <span id="bar-line3" class="menu-line"></span>
            </div>
            <div>
              <ul id="menu">
                <li><a href="#rezerv"><?php echo $text_rezerv;?></a>
                </li>
                <li><a href="#menu"><?php echo $text_menu;?></a>
                </li>
                <li><a href="#gallery"><?php echo $text_gallery;?></a>
                </li>
                <li><a href="#news"><?php echo $text_news;?></a>
                </li>
                <li><a href="#our_team"><?php echo $text_team;?></a>
                </li>
                <li><a href="#contacts"><?php echo $text_contact;?></a>
                </li>
				<li><a id="delivery"><?php echo $text_delivery;?></a>
                </li>
              </ul>
            </div>
          </nav>
          <div class="cart-bag <?php echo $active;?>" id="cart-total">
          <?php echo $cart;?>
          </div>
          <div class="header-number">
              <?php if (!empty($config_contacts)) { ?>
              <a href="tel:<?php echo $config_contacts[0]['phones'][1];?>"><?php echo $config_contacts[0]['phones'][1];?></a>
             <?php unset($config_contacts[0]['phones'][1]);?>
            <div class="hover-number">
              <ul>
                <?php foreach ($config_contacts as $contact) { ?>
                    <?php foreach($contact['phones'] as $phone) { ?>
                    <li><a href="tel:<?php echo $phone;?>"><?php echo $phone;?></a></li>
                    <?php }?>
                <?php }?>
              </ul>
              <span class="btn-call-back" id="callme-button"><?php echo $text_callme;?></span>
            </div>
            <?php }?>
          </div>
          <span class="close-bag-header">Свернуть корзину</span>
        </div>
      </header>
<?php echo $slideshow; ?>
