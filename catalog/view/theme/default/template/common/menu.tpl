        <div class="content-menu">
          <div class="common-title">Меню</div>
          <div class="text-reservations">
            <p><?php echo $text_menu;?></p>
          </div>
          <?php if(isset($categories) && $categories) { ?>
          <div class="box-col">
              <?php $i=1;foreach($categories as $category) { ?>
            <div class="col row-<?php echo $i;?>" data-id="<?php echo $category['category_id'];?>" data-check="<?php echo $category['check'];?>" style="background-image: url(<?php echo $category['image'];?>);">
                <a>
                <span><?php echo $category['name'];?></span>
              </a>
            </div>
              <?php $i++;}?>
          </div>
          <?php }?>
          <span class="height-content-menu">Все меню</span>
        </div>