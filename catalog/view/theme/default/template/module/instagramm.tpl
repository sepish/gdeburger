<?php if($instagramm) { ?>
<div class="instagram-box">
          <div class="box-animait">
            <img src="/image/catalog/project_image/photo_camera.png" class="img-animait" alt="img">
            <img src="/image/catalog/project_image/photo_camera2.png" alt="img">
          </div>
          <div class="box-animait2">
            <img src="/image/catalog/project_image/Phone.png" class="img-animait" alt="img">
            <img src="/image/catalog/project_image/Phone2.png" alt="img">
          </div>
          <div class="title-instagram">Наш Instagram</div>
          <span class="text-instagram">Отмечайте нас на ваших фото или ставьте тег #gdeburger. Будем на связи!</span>
          <div class="content-size">
            <div class="instagram-slide frame" id="forcecentered2">
              <ul class="clearfix">
                  <?php foreach($instagramm as $result) { ?>
                    <li style="background-image:url(<?php echo $result['image'];?>)">
                        <!-- <img src="" alt="<?php echo $result['username'];?>"> -->
                      <div class="info-img-ins">
                        <span>@<?php echo $result['username'];?></span>
                        <p><?php echo $result['text'];?></p>
                      </div>
                    </li>
                  <?php }?>
                  <!-- <li>
                      <img src="<?php //echo $profile_image;?>" alt="#гдебургер">
                      <div class="info-img-ins">
                          <a href="https://www.instagram.com/gdeburger/" style="color: #FFFFFF;">
                            <span>Наш Instagram.</span>
                          </a>
                      </div>
                  </li> -->
              </ul>
            </div>
            <div class="scrollbar">
              <div class="handle">
                <div class="mousearea"></div>
              </div>
            </div>
          </div>
</div>
<?php }?>
