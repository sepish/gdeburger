<div class="news-content">
          <div class="common-title"><?php echo $heading_title; ?></div>
          <div class="content-size">
            <div class="news-slide frame" id="forcecentered3">
              <ul class="clearfix">
                  <?php foreach ($article as $articles) { ?>
                <li>
                    <?php if ($articles['thumb']) { ?>
                  <div class="box_img">
                    <img src="<?php echo $articles['thumb']; ?>" title="<?php echo $articles['name']; ?>" alt="<?php echo $articles['name']; ?>">
                  </div>
                    <?php }?>
                  <div class="info-news">
                    <span><?php echo $articles['name']; ?></span>
                    <?php echo $articles['description']; ?>
                    <span class="btn-news" data-id="<?php echo $articles['article_id']?>"><?php echo $button_more; ?></span>
                  </div>
                </li>
                <?php } ?>
              </ul>
            </div>
            <div class="scrollbar">
              <div class="handle">
                <div class="mousearea"></div>
              </div>
            </div>
          </div>
        </div>