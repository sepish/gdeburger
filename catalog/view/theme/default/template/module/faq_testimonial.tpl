<?php if($success){?>
    <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><?php echo $text_success;?></div>
<?php } ?>

<div class="faq-form">
    <div class="row">
        <form action="/faq" method="POST">
            <div class="form-group col-xs-12 required">
                <div class="col-xs-12">
                    <input type="text" name="author_name" value="<?php echo $author_name;?>" class="form-control default" placeholder="Псевдоним" />
                    <?php if($error_author_name){?>
                        <div class="text-danger"><?php echo $error_author_name;?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group col-xs-12 required">
                <div class="col-xs-12">
                    <input type="text" name="author_mail" value="<?php echo $author_mail; ?>" class="form-control default" placeholder="E-mail" />
                    <?php if($error_author_email){?>
                        <div class="text-danger"><?php echo $error_email;?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group col-xs-12 required">
                <div class="col-xs-12">
                    <textarea name="title" class="form-control default" placeholder="Вопрос"><?php echo $title;?></textarea>
                    <?php if($error_title){?>
                        <div class="text-danger"><?php echo $error_title;?></div>
                    <?php }?>
                </div>
            </div>
            <div class="form-group col-xs-12 required">
                <div class="col-xs-7 pull-left text-right">
                    <input type="submit" value="<?php echo $entry_submit;?>" class="btn btn-primary"/>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="box faq">
    <div class="box-content">
        <?php foreach ($faqs as $faq) { ?>
            <div class="faqlist">
                <div class="box-faqlist" <?php if( !empty($faq['description']) ) { ?>style="width: 90%;"<?php } ?>>
                    <div class="faqlist-name">
                        <i>23.09.2014</i><b><?php echo $faq['author_name']; ?></b>
                    </div>
                    <div class="faqlist-title">
                        <?php echo $faq['title']; ?>
                    </div>
                </div>
                <?php if( !empty($faq['description']) ) { ?>
                    <div class="box-faqrequest">
                        <div class="moderator_name"><b><?php echo $faq['moderator_name']; ?></b><br><i>24.09.2014</i></div>
                        <div class="moderator_description"><?php echo $faq['description']; ?></div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>

<script type="text/javascript" src="catalog/view/javascript/accordion.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.accordion').accordion({defaultOpen: 'some_id'}); //some_id section1 in demo
    });
</script>