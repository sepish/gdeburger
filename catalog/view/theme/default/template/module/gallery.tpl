<link href="catalog/view/theme/default/stylesheet/blueimp/blueimp-gallery.css" type="text/css" rel="stylesheet" media="screen" />
<link href="catalog/view/theme/default/stylesheet/blueimp/blueimp-gallery-indicator.css" type="text/css" rel="stylesheet" media="screen" />
<div class="gallery">
          <div class="common-title">Галерея</div>
          <div class="gallery-slide frame" id="forcecentered">
            <ul class="clearfix thumbnails">
                <?php foreach ($banners as $banner) { ?>
              <li class="image-additional">
                <a href="<?php echo $banner['popup']; ?>" class="thumbnail gallery-element">
                <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>">
                </a>
              </li>
                <?php }?>
            </ul>
          </div>
          <div class="scrollbar">
            <div class="handle">
              <div class="mousearea"></div>
            </div>
          </div>
</div>
 <script src="catalog/view/javascript/blueimp/blueimp-gallery.js" type="text/javascript"></script>
  <script src="catalog/view/javascript/blueimp/blueimp-gallery-fullscreen.js" type="text/javascript"></script>
  <script src="catalog/view/javascript/blueimp/blueimp-gallery-indicator.js" type="text/javascript"></script>
  <script src="catalog/view/javascript/blueimp/jquery.blueimp-gallery.js" type="text/javascript"></script>

<script>
		$(document).on('click', 'a.gallery-element', function (event) {
			event = event || window.event;
			var target = event.target || event.srcElement;
			var link = target.src ? target.parentNode : target;
			var options = {
				index: link,
				event: event,
				fullScreen: false,
				thumbnailIndicators: true,
				carousel: false,
				startSlideshow: false,
				closeOnSwipeUpOrDown: true
			}
			var unique_links = [];
			var links = document.querySelectorAll('a.gallery-element');
			$(links).each(function () {
				var _this = this
				var exists = false;
				$(unique_links).each(function () {
					if ($(_this).attr('href') == $(this).attr('href')) {
						exists = true;
					}
				});
				if (!exists) {
					unique_links.push(this);
				}
			});
			blueimp.Gallery(unique_links, options);
		});
	</script>
	<!-- The Gallery as lightbox dialog, should be a child element of the document body -->
	<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
		<div class="slides"></div>
		<h3 class="title"></h3>
		<a class="prev">‹</a>
		<a class="next">›</a>
		<a class="close">×</a>
		<a class="play-pause"></a>
		<ol class="indicator"></ol>
	</div>