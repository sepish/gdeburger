<?php echo $header; ?>
<div id="main" class="clearfix">
    <div class="img-general cd-background-wrapper">
      <img src="/catalog/view/theme/default/image/img-404.jpg" alt="Ошибка! Страница не найдена!">
      <div class="parallax-wrap cd-floating-background">
        <div class="parallax-box bg-img-parallax"><img src="/catalog/view/theme/default/image/img-404.jpg" alt="Ошибка! Страница не найдена!"></div>
        <div class="error logo parallax-box">
            <a href="/"></a>
            <span class="error-notes">Ошибка! Страница не найдена!</span>
            <a href="/" class="btn-scroll"><span>На Главную<i></i></span></a>
        </div>
      </div>
    </div>
</div>
<style>
    #header,
    #footer{
        display:none;
    }
    #main {
        padding: 0;
    }
</style>
<?php echo $footer; ?>